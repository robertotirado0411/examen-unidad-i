/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenunidadi;

/**
 *
 * @author Edgar Guerrero
 */
public class Contrato {
    private String clave;
    private String puesto;
    private float impuesto;

    public Contrato() {
        this.clave = "";
        this.puesto = "";
        this.impuesto = 0.0f;
    }

    public Contrato(String clave, String puesto, float impuesto) {
        this.clave = clave;
        this.puesto = puesto;
        this.impuesto = impuesto;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(float impuesto) {
        this.impuesto = impuesto;
    }
    
    
    
}
